import RPi.GPIO as GPIO
import time

import paho.mqtt.client as mqtt

servoPIN = 18
GPIO.setmode(GPIO.BCM)
GPIO.setup(servoPIN, GPIO.OUT)

p = GPIO.PWM(servoPIN, 50) # GPIO 17 comme pwm avec une fréquence de 50hz
p.start(angle_to_percent(0)) #Init le servo à 0

#Fonction permettant de convertir un angle en rapport de cycle
def angle_to_percent (angle) :
    #Vérification que l'info entrée en paramètre est valable
    if angle > 180 or angle < 0 :
        return False
    
    start = 4
    end = 12.5
    ratio = (end - start)/180 #Ratio pour le cycle
    angle_as_percent = angle * ratio #Calcul du alpha pour obtenir l'angle désiré

    return start + angle_as_percent #On retourne la valeur alpha pour cet angle

#Fonction pour se connecter au broker
def on_connect(client, userdata, flags, rc):
    print("Connected")
    client.subscribe("raspberry/radar")
    
#Fonction qui sera appelée lors de la réception d'un message
def on_message(client, userdata, msg):
    print(msg.payload)
    command=float(msg.payload) #On converti le msg en float
    p.ChangeDutyCycle(angle_to_percent(int(command))) #On applique le nouvel angle en fonction de la commande reçue

while(1):
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
#On se connecte au broker mojito.homedruon.com sur le port 1883
    client.connect("mojito.homedruon.com", 1883, 60)
    client.loop_forever()
