import RPi.GPIO as GPIO     #Librairie permettant de gérer les pin GPIO du Raspberry
import time     #Permet d'obtenir la notion de temps

GPIO.setmode(GPIO.BCM)

Trig = 23          # Trig sur GPIO 23
Echo = 24         # Echo sur au GPIO 24

GPIO.setup(Trig,GPIO.OUT) #On détermine le pin 23 comme une sortie GPIO
GPIO.setup(Echo,GPIO.IN) #On détermine le pin 24 comme une entrée GPIO

GPIO.output(Trig, False)


##Subscriber topic raspberry/radar
import paho.mqtt.client as mqtt     #bibliothèque qu'on va utilisé pour la communication MQTT


#Fonction connect pour se connecter au broker
def on_connect(client, userdata, flags, rc):
    print("Connected")
    
#Boucle infinie de traitement des données envoyées par le publisher    
while(1):    

    #---Détection de la distance-------#
    #----------------------------------#

    #On crée une impulsion de 0.00001s qu'on envoit sur le trig
    GPIO.output(Trig, True)
    time.sleep(0.00001)
    GPIO.output(Trig, False)

    while GPIO.input(Echo)==0:  ## Emission de l'ultrason on récupère le temps de début d'impulsion
        debutImpulsion = time.time()

    while GPIO.input(Echo)==1:   ## Retour de l'echo on récupère le temps de fin d'impulsion
        finImpulsion = time.time()

    #------------Publisher-----------#
    #--------------------------------#

    #On récupère la distance grâce à cette formule
    distance = round((finImpulsion - debutImpulsion) * 340 * 100 / 2, 1)  ## Vitesse du son = 340 m/s

    #On crée la var client qui sera notre objet client
    client = mqtt.Client()
    client.on_connect = on_connect
    client.connect("mojito.homedruon.com", 1883, 60)    #On se connecte au serveur mojito.homedruon.com sur le port 1883
    
    client.publish('raspberry/radar', payload=distance, qos=0, retain=False)#On publie sur le topic raspberry/radar, le message publié est la distance, on prend un quality of service de 0
    print(distance) #Affichage du message publié
    time.sleep(0.1) #temps d'attente

GPIO.cleanup() #On nettoye nos GPIO

